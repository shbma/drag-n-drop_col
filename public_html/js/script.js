$(document).ready(function () {
    $(".note").attr('draggable', 'true'); //делаем перемещаемыми

    $(".note").each(function () { //назначаем обработчик на начало перемещения
        this.addEventListener('dragstart', handleDragStart, false);
        this.addEventListener('dragend', handleDragEnd, false);
        this.addEventListener('dragenter', handleDragEnter, false);
        this.addEventListener('dragleave', handleDragLeave, false);
        this.addEventListener('dragover', handleDragOver, false);
    });

    function handleDragStart(e) {//обработчик на начало перемещения
        console.warn('dragStart');
        $(this).addClass('above');
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.outerHTML);
        console.log(e.dataTransfer);
        this.style.opacity = '0.4';
        return false;
    }
    function handleDragEnd(e) { //обработчик на конец перемещения
        e.srcElement.style.opacity = '1.0';        
        $(e.srcElement).remove();
        //$(this).removeClass('over');        
        console.warn('dragEnd');console.log(e.dataTransfer);
    }

    $(".box").each(function () { //ставим обработчики событиям контейнера
        this.addEventListener('dragenter', handleDragEnter, false);
        this.addEventListener('dragleave', handleDragLeave, false);
        this.addEventListener('dragover', handleDragOver, false);
        this.addEventListener('drop', handleDrop, false);
    });
    
    $("body").each(function () { //ставим обработчики событиям контейнера
        this.addEventListener('dragenter', handleDragEnter, false);
        this.addEventListener('dragleave', handleDragLeave, false);
        this.addEventListener('dragover', handleDragOver, false);
        this.addEventListener('drop', handleDrop, false);
    });

    function handleDragEnter(e) { //зашли в целевой объект
        $(this).addClass('over');
        e.stopPropagation();
        return false;
    }
    function handleDragLeave(e) { //вышли из целевого объекта
        $(this).removeClass('over');
        e.stopPropagation();
    }
    function handleDragOver(e) { //несем над целевым объектом
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.dataTransfer.dropEffect = 'move';
        e.stopPropagation();
    }
    function handleDrop(e) {//обрабатываем отпускание
        var obj = e.dataTransfer.getData('text/html');        
        $(this).append(obj);
        //назначили новому объекту обработчики перемещений
        $(this).children('.note').last()[0].addEventListener('dragstart', handleDragStart, false);
        $(this).children('.note').last()[0].addEventListener('dragend', handleDragEnd, false);        
        $(this).removeClass('over');           
        console.log($($(this).children('.note').last()[0]).removeClass('above'));
        /*$(".note").each(function () { //назначаем обработчик на начало перемещения
            this.addEventListener('dragstart', handleDragStart, false);
            this.addEventListener('dragend', handleDragEnd, false);
        });*/
        e.stopPropagation();
        console.warn('drop');console.log(e.dataTransfer);
    }
});


